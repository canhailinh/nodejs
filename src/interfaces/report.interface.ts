export interface IReportList{
    name: string,
    date: string,
    status: string
}