const Types = {
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  CLEAR_USER: 'CLEAR_USER',
  LOGIN: 'LOGIN',
  SPLASH: 'SPLASH',

};

export default Types;
