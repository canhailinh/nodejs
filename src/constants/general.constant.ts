export const SIDE = [
    {
        title: 'Buy',
        code: 100
    },
    {
        title: 'Sell',
        code: 101
    }
]
export const INVALID_DATE = 'Invalid date';
export const FORMAT_DATE_TIME_MILLI = 'MMM DD YYYY h:mm:ss.SSS A';

export const RESPONSE_RESULT = {
    success: 1,
    error: 2
}