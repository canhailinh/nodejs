export interface IOrderDropdownModel {
    navigation: string;
    icon: string;
    title: string;
    subTab: IOrderDropdownModel[];
}

