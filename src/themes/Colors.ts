const Colors = {
  transparent: 'transparent',
  blue: '#707070',
  lightBlue: '#C5D9F1'
};

export default Colors;
